import { Stack } from "@mui/material";
import { useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import { setArticlesList, useCounterDispatch } from "../../store/store"
import { Api } from "../api"
import { Article } from "./Article";
import { Loader } from "./Loader";

export interface newes {
    author: string,
    content: string,
    description: string,
    publishedAt: string,
    title: string,
    url: string,
    urlToImage: string
}

const pageSize = 30

 export const Main = ({searchQuery}: {
    searchQuery?: string
 }) => { 
    const [list, setlist] = useState<newes[] | []>([]);
    const [page, setpage] = useState(1);
    const dispatch = useCounterDispatch()
    const newsPerPageFromStore = useSelector((state: any) => state.settings.newsPerPage)
    const count = useRef(0)

    useEffect(() => {
        if (page > 1 && page < count.current) {
            Api({ page: page, searchQuery: searchQuery, newsPerPageFromStore: newsPerPageFromStore }).then((res: any) => {
                setlist([ ...list, ...res.articles ])
                console.log(res.totalResults, 'in Main');
            }).catch(e => console.warn(e.response.data.message))
        }
    }, [page]);

    useEffect(() => {
        Api({ page: page, searchQuery: searchQuery, newsPerPageFromStore: newsPerPageFromStore })
            .then((res: any) => {
                setlist(res.articles)
                count.current = Math.round( res.totalResults / pageSize )
            })
    }, [searchQuery]);

    useEffect(() => {
        if (list.length > 90 && list.length < 120) {
            console.warn("You have requested too many results. Developer accounts are limited to a max of 100 results. You are trying to request results 90 to 120. Please upgrade to a paid plan if you need more results.")
        }
        dispatch(setArticlesList(list))
    }, [list]);
    
    const listRef = useRef(null)
    const scrollHandler = (event: any) => {
        if (event.target.scrollHeight - ( event.target.scrollTop + event.target.clientHeight) < 10) {
            let timer
            clearTimeout(timer)
            timer = setTimeout(() => {
                setpage(page => page + 1)
            }, 200)
        }
    }
    return( <div className="App" style={{overflow: 'auto', height: 'calc(100vh - 64px)'}} onScroll={(e) => scrollHandler(e)}>
        {list.length === 0 && <Loader/>}
        {list && <Stack style={{margin: '20px 40px 20px 40px'}} ref={listRef} >
            {list.map(( article, index ) => <Article article={article} key={index}/>)}
        </Stack>}
    </div>)}