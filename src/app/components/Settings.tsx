import { TextField } from "@mui/material"
import React from "react"
import { useSelector } from "react-redux"
import { useSettingsDispatch, setNewsPerPage } from "../../store/store"

export const Settings = ({
    children,
}: {
    children: any,
}) => {
    const newsPerPageFromStore = useSelector((state: any) => state.settings.newsPerPage)
    const dispatch = useSettingsDispatch()
    
    const changeHandler = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        dispatch(setNewsPerPage(event.target.value))
    }
    return <div>
        <div style={{display: 'flex', alignItems: 'center'}}>
            <h4>Change color mode:</h4>
            <div style={{width: '20px'}}/>
            {children()}
        </div>
        <div style={{display: 'flex', alignItems: 'center'}}>
            <h4>Change news per page:</h4>
            <div style={{width: '20px'}}/>
            <TextField 
                inputProps={{ inputMode: 'numeric', pattern: '[0-9]*' }} 
                defaultValue={newsPerPageFromStore} 
                onChange={(event) => changeHandler(event)}
            />
        </div>
    </div>
}