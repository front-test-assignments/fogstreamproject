import grear from './gear.gif'

export const Loader = () => {
    return <div style={{
        height: '100vh', 
        width: '100%', 
        display: 'flex', 
        justifyContent: 'center', 
        alignContent: 'center'
    }}>
        <img 
            src={grear} 
            style={{alignSelf: 'center'}}
        />
    </div>
}