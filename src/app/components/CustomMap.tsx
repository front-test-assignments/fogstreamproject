import { MapContainer, Marker, Popup, TileLayer, useMapEvents } from "react-leaflet"

export const CustomMap = () => {
return ( <MapContainer 
      center={[51.505, -0.09]} 
      zoom={13} 
      scrollWheelZoom={false}
      style={{height: 'calc(100vh - 100px)', width: '100%'}}
    >
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Marker position={[51.505, -0.09]}>
        <Popup>
          This is a marker.
        </Popup>
      </Marker>
    </MapContainer>)}