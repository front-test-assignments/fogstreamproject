import { Modal, styled, Typography } from "@mui/material"
import { useState } from "react"
import { useSelector } from "react-redux"

export const Card = styled('div')(({ theme }) => ({
    display: 'flex', 
    margin: '10px', 
    backgroundColor: 'beige', 
    borderRadius: '10px',
    opacity: 0.9,
    cursor: 'pointer',
    textDecoration: 'none',
    color: 'black',
    '&:hover': {
        opacity: 1,
    }
}))

export const ArticleSelected = () => {
    const [open, setopen] = useState(false);
    const handleClose = () => setopen(false)
    const handleOpen = () => setopen(true)
    const article = useSelector(( state: any ) => state.articles.selectedArticle)
    if (Object.keys(article).length === 0) return null
    
    return <div>
            <Card >
                <div style={{alignItems: 'center', display: 'flex', paddingLeft: '10px'}}>
                    <img src={article.urlToImage} style={{width: '200px', borderRadius: '10px'}} onClick={handleOpen}/>
                </div>
                <div style={{textAlign: 'start', paddingLeft: '10px', paddingTop: '10px'}}>
                    <Typography variant="h6">{article.author}</Typography>
                    <p>{article.title}</p>
                    <p>{article.content}</p>
                    <p>{article.description}</p>
                </div>
            </Card>
            <Modal
                open={open}
                onClose={() => handleClose()}
            >
                <div style={{height: '100vh', display: 'flex', justifyContent: 'center', alignItems: 'center'}} onClick={handleClose}>
                    <img src={article.urlToImage} style={{width: '600px', borderRadius: '10px', height: 'auto'}}/>
                </div>
            </Modal>
    </div>
}