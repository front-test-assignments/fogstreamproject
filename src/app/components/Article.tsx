import { styled, Typography } from "@mui/material"
import { Link } from "react-router-dom"
import { setSelectedArticle, useNewsDispatch } from "../../store/store"

export const Card = styled(Link)(({ theme }) => ({
    display: 'flex', 
    margin: '10px', 
    backgroundColor: 'beige', 
    borderRadius: '10px',
    opacity: 0.9,
    cursor: 'pointer',
    textDecoration: 'none',
    color: 'black',
    '&:hover': {
        opacity: 1,
    }
}))

export const Article = ({article , key}: {
    article: any,
    key: number
}) => {
    const dispatch = useNewsDispatch()
    const cardClickHandler = () => dispatch(setSelectedArticle(article))
    return (
        <div style={{}} onClick={cardClickHandler}>
            <Card to='/article'>
                <div style={{alignItems: 'center', display: 'flex', paddingLeft: '10px'}}>
                    <img src={article.urlToImage} style={{width: '200px', borderRadius: '10px'}}/>
                </div>
                <div style={{textAlign: 'start', paddingLeft: '10px', paddingTop: '10px'}}>
                    <Typography variant="h6">{article.author}</Typography>
                    <p>{article.title}</p>
                    <p>{article.content}</p>
                    <p>{article.description}</p>
                </div>
            </Card>
            <hr style={{height: '1px solid black'}}/>
        </div>
    )
}
