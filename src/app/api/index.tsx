import axios from "axios";

export const Api = ({ 
    page, 
    searchQuery ,
    newsPerPageFromStore
}:{ 
    page: number, 
    searchQuery?: string ,
    newsPerPageFromStore: number
}) => {
    const query = searchQuery ? searchQuery : 'Bitcoin'
    var url = 'https://newsapi.org/v2/everything?' +
    'q='+query+'&' +
    'sortBy=popularity&' +
    'pageSize='+newsPerPageFromStore+'&' +
    'page='+page+'&' +
    'apiKey=f9a57cfb65ba4233910b9a9678c3accc';

    return axios.get(url).then(res => res.data)
}
// export {Api}