import './App.css'
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from "react-router-dom";
import { Provider } from 'react-redux';
import store from '../store/store';
import { Main, newes } from './components/Main';
import { createContext, useMemo, useState } from 'react';
import { createTheme, PaletteMode, ThemeProvider } from '@mui/material';
import { amber, deepOrange, grey } from '@mui/material/colors';
import Brightness4Icon from '@mui/icons-material/Brightness4';
import Brightness7Icon from '@mui/icons-material/Brightness7';
import IconButton from '@mui/material/IconButton';
import Box from '@mui/material/Box';
import SearchAppBar from './components/AppBar';
import { useSelector } from 'react-redux';
import { CustomMap } from './components/CustomMap';
import { Settings } from './components/Settings';
import { ArticleSelected } from './components/ArticleSelected';

const ColorModeContext = createContext({ toggleColorMode: () => {} });


const getDesignTokens = (mode: PaletteMode) => ({

  palette: {
    mode,
    ...(mode === 'light'
      ? {
          primary: amber,
          divider: amber[200],
          text: {
            primary: grey[900],
            secondary: grey[800],
          },
        }
      : {
          primary: deepOrange,
          divider: deepOrange[700],
          background: {
            default: deepOrange[900],
            paper: deepOrange[900],
          },
          text: {
            primary: '#fff',
            secondary: grey[500],
          },
        }),
  },
});
function App() {
  const [mode, setMode] = useState<PaletteMode>('light');
  const [searchValue, setsearchValue] = useState<string | undefined>(undefined);
  const colorMode = useMemo(
    () => ({
      toggleColorMode: () => {
        setMode((prevMode: PaletteMode) =>
          prevMode === 'light' ? 'dark' : 'light',
        );
      },
    }),
    [],
  );

  const theme = useMemo(() => createTheme(getDesignTokens(mode)), [mode]);

  function SettingsPage() {
    return <Settings children={children}/>
  }
  
  function About() {
    return <CustomMap />
  }
  
  function Help() {
    return <h2>Text informarion</h2>;
  }

  const children = () => (
    <Box
      sx={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 1,
      }}
    >
      {theme.palette.mode} mode
      <IconButton sx={{ ml: 1 }} onClick={colorMode.toggleColorMode} color="inherit">
        {theme.palette.mode === 'dark' ? <Brightness7Icon /> : <Brightness4Icon />}
      </IconButton>
    </Box>
  )

  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <Provider store={store}>
      <Router>
          <SearchAppBar searchValue={searchValue} setsearchValue={setsearchValue} children={children} mainContent={
        <Routes>
          <Route path="/" element={<Main searchQuery={searchValue}/>}/>
          <Route path="/settings" element={<SettingsPage/>}/>
          <Route path="/about" element={<About/>}/>
          <Route path="/help"element={<Help/>}/>
          <Route path="/article"element={<ArticleSelected/>}/>
        </Routes>
          }/>
      </Router>
    </Provider>
      </ThemeProvider>
    </ColorModeContext.Provider>
  )
}

export default App
