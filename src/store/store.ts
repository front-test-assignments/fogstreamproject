import { configureStore, createSlice, PayloadAction } from "@reduxjs/toolkit";
import {
  TypedUseSelectorHook,
  useDispatch,
  useSelector
} from "react-redux";

const counterSlice = createSlice({
  name: "counter",
  initialState: {
    value: 0,
  },
  reducers: {
    increment: (s, action: PayloadAction<number>) => {
      return {
        ...s,
        value: s.value + action.payload
      };
    },
  }
});

const settingsSlice = createSlice({
  name: 'settings',
  initialState: {
    newsPerPage: 21,
  },
  reducers: {
    setNewsPerPage: (s, action: PayloadAction<any>) => {
      return {
        ...s,
        newsPerPage: action.payload
      }
    },
  }
})

const newsSlice = createSlice({
  name: 'news',
  initialState: {
    articles: null,
    selectedArticle: {},
  },
  reducers: {
    setArticlesList: (s, action: PayloadAction<any>) => {
      return {
        ...s,
        articles: s.articles && [ ...s.articles, ...action.payload ]
      }
  },
    setSelectedArticle: (s, action: PayloadAction<any>) => {
      return {
        ...s,
        selectedArticle: action.payload
      }
    },
}})

export const { increment } = counterSlice.actions;
export const { setArticlesList, setSelectedArticle } = newsSlice.actions;
export const { setNewsPerPage } = settingsSlice.actions;

const store = configureStore({
  reducer: {
    counter: counterSlice.reducer,
    articles: newsSlice.reducer,
    settings: settingsSlice.reducer,
  }
});

type RootState = ReturnType<typeof store.getState>;
type AppDispatch = typeof store.dispatch;

export const useCounterDispatch = () => useDispatch<AppDispatch>();
export const useNewsDispatch = () => useDispatch<AppDispatch>();
export const useSettingsDispatch = () => useDispatch<AppDispatch>();

export const useCounterSelector: TypedUseSelectorHook<RootState> = useSelector;
export const useNewsSelector: TypedUseSelectorHook<RootState> = useSelector;
export const useNewsPerPageSelector: TypedUseSelectorHook<RootState> = useSelector;

export default store